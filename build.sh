# Clean build directory
rm -rf build/*

# Build .love
cd loveapp
zip -r ../build/spaceship-earth.love *

cd ../build

# build Windows
cp -R ../love-windows ./spaceship-earth-win
cat ./spaceship-earth-win/love.exe ./spaceship-earth.love > ./spaceship-earth-win/spaceship-earth.exe
rm ./spaceship-earth-win/love.exe
# Build Mac

cp -R /Applications/love.app ../build/Spaceship\ Earth.app
cp ./spaceship-earth.love ./Spaceship\ Earth.app/Contents/Resources/
/usr/libexec/PlistBuddy -c "Set :CFBundleIdentifier org.gloryfish.spaceship-earth" ./Spaceship\ Earth.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Set :CFBundleName Divine Rapier" ./Spaceship\ Earth.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Set :CFBundleSignature org.gloryfish.spaceship-earth" ./Spaceship\ Earth.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Delete :UTExportedTypeDeclarations" ./Spaceship\ Earth.app/Contents/Info.plist
/usr/libexec/PlistBuddy -c "Delete :CFBundleDocumentTypes" ./Spaceship\ Earth.app/Contents/Info.plist

cp ../icon.icns ./Spaceship\ Earth.app/Contents/Resources/Love.icns