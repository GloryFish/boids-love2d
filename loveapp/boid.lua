require 'middleclass'
require 'vector'

Boid = class('Boid')

function Boid:initialize(position)
  self.position = position
  local angle = math.random(0, 2 * math.pi)
  self.velocity =  vector(math.cos(angle), math.sin(angle))
  self.acceleration = vector(0, 0)
  self.size = 10
  self.maxforce = 0.03
  self.maxspeed = 2

  self.accelvec = self.acceleration
end

function Boid:update(dt, boids)
  local separation = self:separation(boids)
  local alignment = self:alignment(boids)
  local cohesion = self:cohesion(boids)

  separation = separation * 1.5
  alignment = alignment * 1.0
  cohesion = cohesion * 1.0

  self:applyForce(separation)
  self:applyForce(alignment)
  self:applyForce(cohesion)

  self.velocity = self.velocity + self.acceleration

  if self.velocity:len() > self.maxspeed then
    self.velocity = self.velocity:normalized() * self.maxspeed
  end

  self.position = self.position + self.velocity * dt * 60

  self.accelvec = self.acceleration

  self.acceleration = self.acceleration * 0
end

function Boid:applyForce(force)
  self.acceleration = self.acceleration + force
end

function Boid:seek(target)
  local desired = target - self.position
  desired = desired:normalized() * self.maxspeed

  local steer = desired - self.velocity
  if steer:len() > self.maxforce then
    steer = steer:normalized() * self.maxforce
  end
  return steer
end

function Boid:draw()
  local direction = self.velocity:normalized() * self.size
  local accel = self.accelvec * 120

  colors.green:set()
  love.graphics.line(self.position.x, self.position.y, self.position.x + direction.x, self.position.y + direction.y)

  colors.red:set()
  love.graphics.line(self.position.x, self.position.y, self.position.x + accel.x, self.position.y + accel.y)


  colors.black:set()
  love.graphics.circle('line', self.position.x, self.position.y, self.size, 15)
end

function Boid:separation(boids)
  local desiredSep = 25
  local steer = vector(0, 0)
  local count = 0

  for index, boid in ipairs(boids) do
    local dist = boid.position:dist(self.position)
    if dist > 0 and dist < desiredSep then
      local diff = self.position - boid.position
      diff = diff:normalized() / dist
      steer = steer + diff
      count = count + 1
    end
  end

  if count > 0 then
    steer = steer / count
  end

  if steer:len() > 0 then
    steer = steer:normalized() * self.maxspeed
    steer = steer - self.velocity

    if steer:len() > self.maxforce then
      steer = steer:normalized() * self.maxforce
    end
  end

  return steer
end

function Boid:cohesion(boids)
  local neighborDist = 50
  local sum = vector(0, 0)
  local count = 0

  for index, boid in ipairs(boids) do
    local dist = boid.position:dist(self.position)
    if dist > 0 and dist < neighborDist then
      sum = sum + boid.position
      count = count + 1
    end
  end

  if count > 0 then
    sum = sum / count
    return self:seek(sum)
  else
    return vector(0, 0)
  end
end

function Boid:alignment(boids)
  local neighborDist = 50
  local sum = vector(0, 0)
  local count = 0

  for index, boid in ipairs(boids) do
    local dist = boid.position:dist(self.position)
    if dist > 0 and dist < neighborDist then
      sum = sum + boid.velocity
      count = count + 1
    end
  end

  if count > 0 then
    sum = sum / count

    sum = sum:normalized()
    sum = sum * self.maxspeed

    local steer = sum - self.velocity

    if steer:len() > self.maxforce then
      steer = steer:normalized() * self.maxforce
    end

    return steer
  else
    return vector(0, 0)
  end
end

