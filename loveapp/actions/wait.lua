require 'middleclass'
require  'actions/action'

ActionWait = class('ActionWait', Action)

function ActionWait:initialize(duration, activity)
  Action.initialize(self)

  self.duration = duration

  self.activity = activity
end

function ActionWait:start()
  self.startTime = love.timer.getTime()
end

function ActionWait:getName()
  return self.activity..' for ' .. tostring(self.duration)..' seconds'
end

function ActionWait:update(dt)
end

function ActionWait:isDone()
  return love.timer.getTime() - self.startTime > self.duration
end