--
--  base.lua
--  rogue-descent
--
--  A base scene that can be used when creating new scenes.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'entity'
require 'tableextras'
require 'boid'
require 'rectangle'

local scene = Gamestate.new()

function scene:enter(pre)
  self.boids = {}

  self.bounds = Rectangle(vector(0, 0), vector(love.graphics.getWidth(), love.graphics.getHeight()))

  for i = 0, 100 do
    local boid = Boid(vector(math.random(0, love.graphics.getWidth()), math.random(0, love.graphics.getHeight())))
    table.insert(self.boids, boid)
  end

end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  end
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
end

function scene:update(dt)
  for index, boid in ipairs(self.boids) do
    boid:update(dt, self.boids)

    local max = self.bounds:getMax()
    local min = self.bounds:getMin()

    -- Wrap around
    if boid.position.x < -boid.size then
        boid.position.x = max.x + boid.size
    end
    if boid.position.y < -boid.size then
        boid.position.y = max.y + boid.size
    end
    if boid.position.x > max.x + boid.size then
        boid.position.x = min.x - boid.size
    end
    if boid.position.y > max.y + boid.size then
        boid.position.y = min.y - boid.size
    end


  end

end

function scene:draw()
  for index, boid in ipairs(self.boids) do
    boid:draw()
  end
end

function scene:quit()
end

function scene:leave()
end

return scene