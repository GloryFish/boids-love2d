--
--  base.lua
--  rogue-descent
--
--  A base scene that can be used when creating new scenes.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'
require 'textbutton'

local scene = Gamestate.new()

function scene:enter(pre)
  self.prayButton = TextButton('Pray')
  self.prayButton.position = vector(100, 400)

  self.voteButton = TextButton('Vote')
  self.voteButton.position = vector(300, 400)

end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  end
end

function scene:mousepressed(x, y, button)
  self.prayButton:mousepressed(vector(x, y))
  self.voteButton:mousepressed(vector(x, y))
end

function scene:mousereleased(x, y, button)
  self.prayButton:mousereleased(vector(x, y))
  self.voteButton:mousereleased(vector(x, y))
end

function scene:update(dt)
  if love.mouse.isDown('l') then
  end
end

function scene:draw()
  self.prayButton:draw()
  self.voteButton:draw()
end

function scene:quit()
end

function scene:leave()
end

return scene