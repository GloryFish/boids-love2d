--
--  base.lua
--  rogue-descent
--
--  A base scene that can be used when creating new scenes.
--
--  Created by Jay Roberts on 2012-02-23.
--  Copyright 2012 Jay Roberts. All rights reserved.
--

require 'logger'
require 'vector'
require 'colors'
require 'rectangle'

local scene = Gamestate.new()

function scene:enter(pre)
  -- Play title drone
  self.music = love.audio.newSource('resources/sounds/drone.mp3', 'stream')
  self.music:setLooping('true')
  -- love.audio.play(self.music)


  self.fullscreen = false

  local modes = love.graphics.getModes()
  table.sort(modes, function(a, b) return a.width*a.height < b.width*b.height end)
  self.fullscreenMode = modes[#modes]


end

function scene:keypressed(key, unicode)
  if key == 'escape' then
    self:quit()
  end

  if key == ' ' then
    self:startGame()
  end

  if key == 'f' then
    self.fullscreen = not self.fullscreen

    if self.fullscreen then
      love.graphics.setMode(self.fullscreenMode.width, self.fullscreenMode.height, true, true, 0)
      Canvases = {
        buffer = love.graphics.newCanvas(self.fullscreenMode.width, self.fullscreenMode.height),
      }
    else
      love.graphics.setMode(1024, 768, false, true, 0)
      Canvases = {
        buffer = love.graphics.newCanvas(),
      }
    end

    Notifier:postMessage('window_resize')
  end
end

function scene:mousepressed(x, y, button)
end

function scene:mousereleased(x, y, button)
end

function scene:update(dt)
  Timer.update(dt)
  self:startGame()
end

function scene:draw()
  love.graphics.print('Press space to continue.', 30, 30)
  love.graphics.print('Press "F" to toggle fullscreen.', 30, 60)
end

function scene:startGame()
  local fadeOutTime = 0

  local startTime = love.timer.getTime()
  Timer.do_for(fadeOutTime, function(dt)
    local progress = (love.timer.getTime() - startTime) / fadeOutTime
    self.music:setVolume(1 - progress)
  end,
  function()
    self.music:setVolume(0)
    Gamestate.switch(scenes.game)
  end)
end


function scene:quit()
  love.event.push('quit')
end

function scene:leave()
end

return scene