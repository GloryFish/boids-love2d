require 'middleclass'

local StateRunAction = class('StateRunAction')

function StateRunAction:initalize()
end

function StateRunAction:enter(agent)
  self.agent = agent
end

function StateRunAction:update(dt)
  if #self.agent.actions == 0 then
    self.agent:setState(States:getState('select_action'))
  else
    local current_action = self.agent.actions[1]
    if not current_action.started then
      current_action.started = true
      current_action:start()
    end
    current_action:update(dt)
    if current_action:isDone() then
      table.remove(self.agent.actions, 1)
    end
  end

  -- Dead
  if self.agent:getHealth() == 0 then
    self.agent:setState(States:getState('dead'))
  end
end

function StateRunAction:exit()
end

return StateRunAction
