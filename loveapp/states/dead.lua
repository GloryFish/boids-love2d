require 'middleclass'
require 'notifier'

local StateDead = class('StateDead')

function StateDead:initalize()
end

function StateDead:enter(agent)
  Notifier:postMessage('station_console_log', string.format('%s has left the station', agent:getName()))
  Notifier:postMessage('agent_died', agent)
  agent:setAnimation('agent_dead')
end

function StateDead:update(dt)
end

function StateDead:exit()
end

return StateDead
