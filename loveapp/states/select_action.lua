require 'middleclass'

local StateSelectAction = class('StateSelectAction')

function StateSelectAction:initalize()
end

function StateSelectAction:enter(agent)
  self.agent = agent
end

function StateSelectAction:update(dt)
  -- if the agent has no actions
    -- Get a list of advertisements
    local ads = self.agent.station:getAdvertisements()

    print(self.agent.name..' get ads')

    if #ads > 0 then
      -- score the advertisements
      local bestScore = 0
      local bestAd = nil
      for index, ad in ipairs(ads) do
        local score = nil
        if ad.score then
          print(ad.name..' static score of '..ad.score)
          score = ad.score
        else
          score = self.agent:scoreForRewards(ad.rewards)
        end

        if score > bestScore then
          bestScore = score
          bestAd = ad
        end
      end

      if bestAd == nil then
        bestAd = ads[math.random(#ads)]
      end

      -- Select the best advertisement
      local selected_ad = bestAd -- Just pick the first one found

      -- Place the advertisement's actions onto the queue

      for index, action in ipairs(selected_ad.actions) do
        self.agent:addAction(action)
      end

      -- Change to a RunActions state
      self.agent:setState(States:getState('run_action'))
    else
      -- show frustration
      -- log a message for the greatest need like "can't find something to eat!"
      -- based on certain need criteria change to a state for a more basic action like: wander,
      -- sit and cry, etc <-- these should mostly be handled by hidden, low priority needs (boredom?)
    end
end

function StateSelectAction:exit()
end

return StateSelectAction
