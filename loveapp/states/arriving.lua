require 'middleclass'

local StateArriving = class('StateArriving')

function StateArriving:initalize()

end

function StateArriving:enter(agent)
  self.agent = agent

  print('arriving')
  Timer.add(5, function()
    print('arrived')
    self.agent:setState(States:getState('select_action'))
  end)
end

function StateArriving:update(dt)
end

function StateArriving:exit()
end

return StateArriving
