require 'middleclass'
require 'ui/statbar'
require 'notifier'

AgentInfoFrame = class('AgentInfoFrame')

function AgentInfoFrame:initialize()
  self.frame = nil
  self.agent = nil
  self.needsLabels = {}
  self.happinessLabel = nil
  self.healthLabel = nil
  self.actionsLabel = nil
end

function AgentInfoFrame:showForAgent(agent)
  if self.agent == agent then
    return
  end

  self.agent = agent

  if self.frame ~= nil then
    self.frame:Remove()
  end

  local frame = loveframes.Create('frame')
  frame:SetSize(140, 210)
  frame:Center()
  frame:SetPos(15, 15)
  frame:SetName(string.format('%s Info', agent:getName()))

  frame.OnClose = function()
    self.frame = nil
    self.agent = nil
    self.needsLabels = {}
    Notifier:stopListeningForMessage('ui_update', self)
  end

  local list = loveframes.Create('list', frame)
  list:SetPos(0, 25)
  list:SetSize(frame:GetWidth(), frame:GetHeight() - 25)
  list:SetDisplayType('vertical')
  list:SetPadding(5)
  list:SetSpacing(5)

  for name, need in pairs(self.agent.needs) do
    local text = loveframes.Create('text')
    text:SetText(string.format('%s: %s', name, tostring(need)))
    self.needsLabels[name] = text
    list:AddItem(text)
  end

  self.happinessLabel = loveframes.Create('text')
  self.happinessLabel:SetText(string.format('Happiness: %i', self.agent:getHappiness()))
  list:AddItem(self.happinessLabel)

  self.healthLabel = loveframes.Create('text')
  self.healthLabel:SetText(string.format('Health: %i', self.agent:getHappiness()))
  list:AddItem(self.healthLabel)

  self.actionsLabel = loveframes.Create('text')
  local actions = ''
  for index, action in ipairs(self.agent.actions) do
    actions = actions .. action:getName() .. ', '
  end
  self.actionsLabel:SetText(actions)
  list:AddItem(self.actionsLabel)

  self.frame = frame

  self:updateInfo()

  Notifier:listenForMessage('ui_update', self)
end

function AgentInfoFrame:receiveMessage(message, data)
  if message == 'ui_update' then
    local dt = data
    self:updateInfo()
  end
end

function AgentInfoFrame:updateInfo()
  for name, need in pairs(self.agent.needs) do
    self.needsLabels[name]:SetText(string.format('%s: %s', name, tostring(need)))
  end

  self.happinessLabel:SetText(string.format('Happiness: %i', self.agent:getHappiness()))
  self.healthLabel:SetText(string.format('Health: %i', self.agent:getHealth()))

  local actions_text = ''
  for index, action in ipairs(self.agent.actions) do
    actions_text = actions_text .. action:getName() .. ', '
  end
  self.actionsLabel:SetText(actions_text)
end