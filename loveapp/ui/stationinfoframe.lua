require 'middleclass'
require 'ui/statbar'
require 'notifier'

StationInfoFrame = class('StationInfoFrame')

function StationInfoFrame:initialize()
  self.frame = nil
  self.station = nil
  self.powerLabel = nil
  self.powerSlider = nil
  self.powerPriorityLabel = nil
end

function StationInfoFrame:showForStation(station)
  if self.station == station then
    return
  end

  self.station = station

  if self.frame ~= nil then
    self.frame:Remove()
  end

  local frame = loveframes.Create('frame')
  frame:SetSize(160, 120)
  frame:Center()
  frame:SetPos(200, 15)
  frame:SetName(string.format('Station Info'))

  frame.OnClose = function()
    self.frame = nil
    self.station = nil
    Notifier:stopListeningForMessage('ui_update', self)
  end

  local list = loveframes.Create('list', frame)
  list:SetPos(0, 25)
  list:SetSize(frame:GetWidth(), frame:GetHeight() - 25)
  list:SetDisplayType('vertical')
  list:SetPadding(5)
  list:SetSpacing(5)

  self.powerLabel = loveframes.Create('text')
  self.powerLabel:SetText(string.format('Power: %i', self.station:getPower()))
  list:AddItem(self.powerLabel)

  self.powerSlider = loveframes.Create('slider')
  self.powerSlider:SetMinMax(1, 4)
  self.powerSlider:SetValue(self.station:getPowerPriority())
  self.powerSlider:SetText('Priority')
  self.powerSlider:SetDecimals(0)
  self.powerSlider.OnValueChanged = function(slider)
    self.station:setPowerPriority(slider:GetValue())
    Notifier:postMessage('station_conole_log', 'Power production priority is now: %i', slider:GetValue())
  end

  list:AddItem(self.powerSlider)

  self.powerPriorityLabel = loveframes.Create('text')
  self.powerPriorityLabel:SetText(string.format('Priority: %i', self.station:getPowerPriority()))
  list:AddItem(self.powerPriorityLabel)

  -- Slider for effect
  local effectSlider = loveframes.Create('slider')
  effectSlider:SetMinMax(0, 1)
  effectSlider:SetValue(0)
  effectSlider:SetText('Effect Amount')
  effectSlider:SetDecimals(5)
  effectSlider.OnValueChanged = function(slider)
    Shaders.amount = slider:GetValue()
  end

  list:AddItem(effectSlider)

  self.frame = frame

  self:updateInfo()

  Notifier:listenForMessage('ui_update', self)
end

function StationInfoFrame:receiveMessage(message, data)
  if message == 'ui_update' then
    local dt = data
    self:updateInfo()
  end
end

function StationInfoFrame:updateInfo()
  self.powerLabel:SetText(string.format('Power: %i (Max: %i)', self.station:getPower(), self.station:getPowerCap()))
  self.powerPriorityLabel:SetText(string.format('Priority: %i', self.station:getPowerPriority()))
end