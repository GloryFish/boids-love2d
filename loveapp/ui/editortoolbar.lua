require 'middleclass'
require 'notifier'

EditorToolbar = class('EditorToolbar')

function EditorToolbar:initialize()
  local frame = loveframes.Create('frame')
  frame:SetSize(140, 70)
  frame:Center()
  frame:SetPos(love.graphics.getWidth() - 85, 15)
  frame:SetName('Editor')

  frame.OnClose = function()
    self.frame = nil
    self.agent = nil
    self.needsLabels = {}
    Notifier:stopListeningForMessage('ui_update', self)
  end

  self.frame = frame

  local list = loveframes.Create('list', frame)
  list:SetPos(0, 25)
  list:SetSize(frame:GetWidth(), frame:GetHeight() - 25)
  list:SetDisplayType('horizontal')
  list:SetPadding(5)
  list:SetSpacing(5)

  Notifier:listenForMessage('ui_update', self)
end

function EditorToolbar:update(dt)

end

function EditorToolbar:receiveMessage(message, data)
  if message == 'ui_update' then
    self:update(dt)
  end
end