require 'middleclass'
require 'ui/statbar'
require 'notifier'

MenuBar = class('MenuBar')

function MenuBar:initialize()
  self.station = nil
  self.list = nil
  self.powerLabel = nil
end

function MenuBar:setStation(station)
  self.station = station
end

function MenuBar:show()
  local list = loveframes.Create('list', frame)
  list:SetPos(0, love.graphics.getHeight() - 70)
  list:SetSize(love.graphics.getWidth(), 50)
  list:SetDisplayType('horizontal')
  list:SetPadding(0)
  list:SetSpacing(0)

  self.powerLabel = loveframes.Create('text')
  self.powerLabel:SetText(string.format('Power: %i', self.station:getPower()))
  list:AddItem(self.powerLabel)

  local spacer = loveframes.Create('panel')
  spacer:SetWidth(200)
  list:AddItem(spacer)

  local buttonConsole = loveframes.Create('button')
  buttonConsole:SetSize(150, 50)
  buttonConsole:SetText('Console')
  buttonConsole.OnClick = function()
    print('console')
    Notifier:postMessage('set_view_mode', 'console')
  end
  list:AddItem(buttonConsole)

  local buttonVideo = loveframes.Create('button')
  buttonVideo:SetSize(150, 50)
  buttonVideo:SetText('Video')
  buttonVideo.OnClick = function()
    print('video')
    Notifier:postMessage('set_view_mode', 'video')
  end
  list:AddItem(buttonVideo)

  local spacer2 = loveframes.Create('panel')
  spacer2:SetWidth(100)
  list:AddItem(spacer2)

  local buttonMetrics = loveframes.Create('button')
  buttonMetrics:SetSize(150, 50)
  buttonMetrics:SetText('Metrics')
  buttonMetrics.OnClick = function()
    print('metrics')
    Notifier:postMessage('set_view_mode', 'metrics')
  end
  list:AddItem(buttonMetrics)

  local buttonRepair = loveframes.Create('button')
  buttonRepair:SetSize(150, 50)
  buttonRepair:SetText('Repair')
  buttonRepair.OnClick = function()
    print('repair')
    Notifier:postMessage('set_view_mode', 'repair')
  end
  list:AddItem(buttonRepair)



  self.list = list

  self:updateInfo()

  Notifier:listenForMessage('ui_update', self)
end

function MenuBar:receiveMessage(message, data)
  if message == 'ui_update' then
    local dt = data
    self:updateInfo()
  end
end

function MenuBar:updateInfo()
  self.powerLabel:SetText(string.format('Power: %i', self.station:getPower()))
end