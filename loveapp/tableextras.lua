--
--  tableextras.lua
--  ExperimentalGameplayProject-Feb-2011
--
--  Created by Jay Roberts on 2011-02-07.
--  Copyright 2011 GloryFish.org. All rights reserved.
--

function table.contains(tbl, element)
  for k, v in pairs(tbl) do
    if v == element then
      return true
    end
  end
  return false
end

function table.reduce(tbl, fn)
    local acc
    for k, v in ipairs(tbl) do
        if 1 == k then
            acc = fn(0, v)
        else
            acc = fn(acc, v)
        end
    end
    return acc
end