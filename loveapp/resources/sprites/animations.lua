local animations = {
  entity_default = {
    rotation = 0,
    frames = {
      {
        name = 'object_rock.png',
        duration = 100,
      }
    },
  },

  entity_bed_idle = {
    rotation = 0,
    frames = {
      {
        name = 'object_bed.png',
        duration = 100,
      }
    },
  },

  entity_bicycle_idle = {
    rotation = 0,
    frames = {
      {
        name = 'object_bicycle.png',
        duration = 100,
      }
    },
  },

  entity_fridge_idle = {
    rotation = 0,
    frames = {
      {
        name = 'object_fridge.png',
        duration = 100,
      }
    },
  },

  entity_game_idle = {
    rotation = 0,
    frames = {
      {
        name = 'object_game.png',
        duration = 100,
      }
    },
  },

  entity_toilet_idle = {
    rotation = 0,
    frames = {
      {
        name = 'object_toilet.png',
        duration = 100,
      }
    },
  },

  agent_idle = {
    rotation = 0,
    frames = {
      {
        name = 'agent_neutral.png',
        duration = 5,
      },
    },
  },

  agent_dead = {
    rotation = 0,
    frames = {
      {
        name = 'agent_frown.png',
        duration = 5,
      },
    },
  },
}

return animations