-- Apple

require 'middleclass'
require 'actions/actions'

local EntityFridge = class('EntityFridge', Entity)


function EntityFridge:initialize()
  Entity.initialize(self)

  self.animationName = 'entity_fridge_idle'
end

function EntityFridge:getAdvertisements()
  local ads = {
    {
      name = 'eat',
      rewards = {
        hunger = 30,
      },
      actions = {
        ActionMoveTo(self.position),
        ActionWait(2, 'Get food'),
        ActionWait(5, 'Make snack'),
        ActionCustom('Eat snack',
          function(this, dt) -- update
            this.agent:changeNeed('hunger', 30)
            this.agent:changeNeed('bathroom', -5)
          end,
          function() -- done
            return true
          end
         ),
      }
    },
  }

  return ads
end

function EntityFridge:draw()
  Entity.draw(self)
end

return EntityFridge