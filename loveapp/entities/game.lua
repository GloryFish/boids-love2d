-- Apple

require 'middleclass'
require 'actions/actions'

local EntityGame = class('EntityGame', Entity)


function EntityGame:initialize()
  Entity.initialize(self)

  self.animationName = 'entity_game_idle'
end

function EntityGame:getAdvertisements()
  local ads = {
    {
      name = 'play',
      rewards = {
        boredom = 25,
        fatigue = 5,
      },
      actions = {
        ActionMoveTo(self.position),
        ActionWait(5, 'Playing'),
        ActionCustom('Play',
          function(this, dt) -- update
            this.agent:changeNeed('boredom', 20)
            this.agent:changeNeed('fatigue', 5)
          end,
          function() -- done
            return true
          end
         ),
      }
    },
  }

  return ads
end

function EntityGame:draw()
  Entity.draw(self)
end

return EntityGame