-- Apple

require 'middleclass'
require 'actions/actions'

local EntityToilet = class('EntityToilet', Entity)


function EntityToilet:initialize()
  Entity.initialize(self)

  self.animationName = 'entity_toilet_idle'
end

function EntityToilet:getAdvertisements()
  local ads = {
    {
      name = 'pee',
      rewards = {
        bathroom = 100,
      },
      actions = {
        ActionMoveTo(self.position),
        ActionWait(5, 'Use toilet'),
        ActionCustom('Pee',
          function(this, dt) -- update
            this.agent:changeNeed('bathroom', 100)
          end,
          function() -- done
            return true
          end
         ),
      }
    },
  }

  return ads
end

function EntityToilet:draw()
  Entity.draw(self)
end

return EntityToilet