require 'middleclass'
require 'vector'
require 'colors'
require 'rectangle'
require 'room'
require 'utility'
require 'agent'
require 'notifier'

Station = class('Station')

function Station:initialize()
  self.rooms = {}

  --
  -- Living Room
  --
  local livingRoom = Room('Living Room', vector(1024, 300))
  livingRoom:setPosition(vector(0, 0))
  livingRoom:setActive(true)
  table.insert(self.rooms, livingRoom)

  local ent1 = Entity.static.create('bed')
  ent1:setStation(self)
  livingRoom:addEntity(ent1)
  ent1:setPosition(vector(60, 190))

  local ent2 = Entity.static.create('fridge')
  ent2:setStation(self)
  livingRoom:addEntity(ent2)
  ent2:setPosition(vector(512, 190))

  local ent3 = Entity.static.create('toilet')
  ent3:setStation(self)
  livingRoom:addEntity(ent3)
  ent3:setPosition(vector(964, 190))

  local ent4 = Entity.static.create('bicycle')
  ent4:setStation(self)
  livingRoom:addEntity(ent4)
  ent4:setPosition(vector(774, 190))

  local ent5 = Entity.static.create('game')
  ent5:setStation(self)
  livingRoom:addEntity(ent5)
  ent5:setPosition(vector(250, 190))

  --
  -- Science Lab
  --
  local scienceLab = Room('Science Lab', vector(512, 300))
  scienceLab:setPosition(vector(-512, 0))
  table.insert(self.rooms, scienceLab)

  --
  -- Workshop
  --
  local workshop = Room('workshop', vector(512, 300))
  workshop:setPosition(vector(1024, 0))
  table.insert(self.rooms, workshop)

  -- Upgrades
  self.upgrades = require 'resources/data/upgrades'

  -- Station stats
  self.power = 0
  self.powerCap = 100
  self.powerPriority = 2 -- 1 = never, 2 = low priority, 3 = high priority, 4 = always
  self.powerScores = {}
  self.powerScores[1] = -math.huge;
  self.powerScores[2] = 10 / 90 - 10 / 100; -- Equivalent to a reward that increases one need from 90 -> 100
  self.powerScores[3] = 10 / 40 - 10 / 100; -- Equivalent to a reward that increases one need from 40 -> 100
  self.powerScores[4] = math.huge;

  self.agents = {}

  Notifier:listenForMessage('agent_died', self)
end

function Station:receiveMessage(message, data)
  if message == 'agent_died' then
    local agent = data
    Timer.add(3, function()
      self:spawnAgent()
    end)
  end
end

function Station:addEntity(entity)
  table.insert(self.entities, entity)
end

function Station:getAdvertisements()
  local advertisments = {}

  for _, room in ipairs(self.rooms) do
    if room:getActive() then
      for _, entity in ipairs(room:getEntities()) do
        for _, ad in ipairs(entity:getAdvertisements()) do
          table.insert(advertisments, ad)
        end
      end
    end
  end
  return advertisments
end

function Station:getPower()
  return self.power
end

function Station:setPowerCap(cap)
  self.powerCap = cap
end

function Station:getPowerCap()
  return self.powerCap
end

function Station:getPowerPriority()
  return self.powerPriority
end

function Station:setPowerPriority(priority)
  self.powerPriority = priority
end

function Station:getPowerScore()
  return self.powerScores[self.powerPriority]
end

function Station:enableUpgrade(name)
  if self.upgrades[name] == nil then
    print('Invalid upgrade '..name)
    return
  end

  local upgrade = self.upgrades[name]

  for index, requirement in ipairs(upgrade.requirements) do
    if not self.upgrades[requirement].enabled then
      print(string.format('Required upgrade %s not enabled', requirement))
      return
    end
  end

  self.upgrades[name].enable(self)
  self.upgrades[name].enabled = true
end

function Station:spawnAgent()
  local agent = Agent(self)
  agent:setPosition(vector(490, 200))

  self.agents[agent.id] = agent

  Notifier:postMessage('station_console_log', string.format('%s has arrived.', agent:getName()))
  Notifier:postMessage('agent_spawned', agent)

  return agent
end

function Station:changePower(amount)
  self.power = math.clamp(self.power + amount, 0, self.powerCap)
end

function Station:floorHeightAtLocation(location)
  local room = self:roomAtLocation(location)
  if room then
    return room:floorHeightAtLocation(location)
  end
  return 0
end

function Station:roomAtLocation(location)
  for index, room in ipairs(self.rooms) do
    if room:containsLocation(location) then
      return room
    end
  end
end

function Station:update(dt)
  for index, agent in ipairs(self.agents) do
    agent:update(dt)
  end
end

function Station:draw()
  for index, room in ipairs(self.rooms) do
    room:draw()
  end

  for index, agent in ipairs(self.agents) do
    agent:draw()
  end
end
