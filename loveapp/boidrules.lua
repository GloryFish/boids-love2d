require 'middleclass'


BoidRules = class('BoidRules')

function BoidRules:rule1(boids, boid)
  -- Rule 1: Boids try to fly towards the center of mass of neighboring boids.
  local sum = vector(0, 0)
  for index, b in ipairs(boids) do
    if b ~= boid then
      sum = sum + b.position
    end
  end

  local centroid = sum / (#boids - 1)
  return (centroid - boid.position) / 100
end

function BoidRules:rule2(boids, boid)
  -- Rule 2: Boids try to keep a small distance away from other objects (including other boids).
  local c = vector(0, 0)

  for index, b in ipairs(boids) do
    if b ~= boid then
      if boid.position:dist(b.position) < 50 then
        c = c - (b.position - boid.position)
      end
    end
  end

  return c / (#boids - 1)
end


function BoidRules:rule3(boids, boid)
  -- Rule 3: Boids try to match velocity with near boids.
  local pv = vector(0, 0)

  for index, b in ipairs(boids) do
    if b ~= boid then
        pv = pv + b.velocity
    end
  end

  pv = pv / (#boids - 1)

  return (pv - boid.velocity) / 8
end


function BoidRules:position(boids, boid, position)
  return vector(0, 0)
end

function BoidRules:bounds(boids, boid, bounds)
  local v = vector(0, 0)
  local min = bounds:getMin()
  local max = bounds:getMax()

  if boid.position.x < min.x then
    v.x = 10
  elseif boid.position.x > max.x then
    v.x = -10
  end

  if boid.position.y < min.y then
    v.y = 10
  elseif boid.position.y > max.y then
    v.y = -10
  end

  return v
end