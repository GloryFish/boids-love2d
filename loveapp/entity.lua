require 'vector'
require 'states'

-- Load entity prototype data

Entity = class('Entity')

function Entity:initialize()
  self.spritesheet = Sprites.main

  -- Size and position
  self.position = vector(0, 0)
  self.rotation = 0
  self.scale = 1
  self.flip = 1
  self.offset = vector(0, 0)

  -- Animation
  self.currentFrameIndex = 1
  self.animationName = 'entity_default'
  self.elapsed = 0

  -- Advertisements
  self.advertisements = {}
end

function Entity:setPosition(pos)
  self.position = pos
end

function Entity:setStation(station)
  self.station = station
end

function Entity:getCurrentSize()
  local animation = self.spritesheet.animations[self.animationName]
  local currentFrame = animation.frames[self.currentFrameIndex]
  local quad = self.spritesheet.quads[currentFrame.name]
  local x, y, w, h = quad:getViewport()
  return vector(w, h)
end

function Entity:getAdvertisements()
  return self.advertisements
end

function Entity:update(dt)
  -- Handle animation
  self.elapsed = self.elapsed + dt
  local animation = self.spritesheet.animations[self.animationName]
  if #animation.frames > 1 then -- More than one frame
    local duration = animation.frames[self.currentFrameIndex].duration

    if self.elapsed > duration then -- Switch to next frame
      self.currentFrameIndex = self.currentFrameIndex + 1
      if self.currentFrameIndex > #animation.frames then -- Aaaand back around
        self.currentFrameIndex = 1
      end
      self.elapsed = self.elapsed - duration
    end
  end
end

function Entity:tick()
end

function Entity:draw()
  colors.white:set()

  local animation = self.spritesheet.animations[self.animationName]
  local currentFrame = animation.frames[self.currentFrameIndex]
  self.spritesheet.batch:addq(self.spritesheet.quads[currentFrame.name],
                              math.floor(self.position.x),
                              math.floor(self.position.y),
                              animation.rotation - self.rotation,
                              self.scale * self.flip,
                              self.scale,
                              self.offset.x,
                              self.offset.y)
end

function Entity.static.create(name)
  local entity = require('entities/'..name)
  return entity()
end


